# README #

### What is this repository for? ###

* APEX Code for Force Academy LA 2017 Trigger Workout session

### Summary ###

* AccountBad - Bad Trigger
* AccountGood - Good Trigger
* AccountAllHelper - Helper/Handler Class for AccountGood
* AccountAllTest - APEX Test Class with Limits class to output performance data